import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import MicroNutrients from './components/microNutrients/MicroNutrients';
import Home from './components/home/Home';
import Fruits from './components/fruits/Fruits';
import NavBar from './partials/navbar/NavBar';

function App() {
  return (
    <Router>
      <NavBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/micro-nutrients" element={<MicroNutrients />} />
        <Route path="/fruits" element={<Fruits />} />
      </Routes>
    </Router>
  );
}

export default App;
