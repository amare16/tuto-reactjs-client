import React, { useState, useEffect } from "react";
import axios from "axios";
import ListGroup from "react-bootstrap/ListGroup";
import { Card, Col, Container, Row } from "react-bootstrap";
import { URL } from '../../url';
import "./fruits.css";

const Fruits = () => {
  const [fruits, setFruits] = useState([]);
  const [selectedFruitFamily, setSelectedFruitFamily] = useState(fruits);

  
  useEffect(() => {
    // fetch data
    getAllFruits();
  }, []);

  const getAllFruits = () => {
    axios
      .get(`${URL}/fruits`)
      .then((response) => {
        const allFruits = response.data;
       // console.log("data from api: ", allFruits);

        // add our data to state
        setFruits(allFruits);
        setSelectedFruitFamily(allFruits);
      })
      .catch((error) => console.error(`Error: ${error}`));
  };

  const handleSelect = (e) => {
    // option value when selected
    const selectedValue = e.target.value;

    // store selected value in array
    let result = [];
    result = fruits.filter((data) => {
      if (selectedValue === "All") {
        return selectedFruitFamily;
      }
      return data.family.search(selectedValue) != -1;
      
      
    });

    setSelectedFruitFamily(result);
    
  };
  let removeDuplicateElement = Array.from(new Set(selectedFruitFamily.map(selected => selected.family)))
  

  return (
    <Container>
      <Row className="cardRow">
        <Col md={3}>
        <select
            value={removeDuplicateElement}
            onChange={handleSelect}
            className="form-select mx-auto form-select-md mb-3 bg-warning"
            aria-label=".form-select-lg example"
          >
            <option value="All">All</option>
           
            {removeDuplicateElement.map((fruitFamily, i) => (
              <option key={i} value={fruitFamily}>
                {fruitFamily}
              </option>
            ))}
          </select>
        </Col>
        <Col md={9} className="cardCol">
          {selectedFruitFamily.map((fruit, index) => {
            return (
              <Card key={index} className="card" style={{ width: "14rem" }}>
                <Card.Img
                  style={{objectFit: "cover", width: "100%", height: "40vh"}}
                  variant="top"
                  src={fruit.picture}
                  className="cardImage"
                />

                <Card.Body>
                  <Card.Title className="cardTitle text-center">
                    {fruit.name}
                  </Card.Title>
                  <Card.Text className="cardText">
                    Family: {fruit.family}
                  </Card.Text>
                  <Card.Text className="cardText">
                    Genus: {fruit.genus}
                  </Card.Text>
                  <Card.Text className="cardText">
                    Order: {fruit.order}
                  </Card.Text>
                  <Card.Title className="cardNutritions text-center">Nutritions</Card.Title>
                  <ListGroup>
                    <ListGroup.Item variant="success" className="cardText">
                      Calories: {fruit.nutritions["calories"]}
                    </ListGroup.Item>
                    <ListGroup.Item variant="primary" className="cardText">
                      Carbohydrates: {fruit.nutritions["carbohydrates"]}
                    </ListGroup.Item>
                    <ListGroup.Item variant="info" className="cardText">
                      Protein: {fruit.nutritions["protein"]}
                    </ListGroup.Item>
                    <ListGroup.Item variant="danger" className="cardText">
                      Fat: {fruit.nutritions["fat"]}
                    </ListGroup.Item>
                    <ListGroup.Item variant="secondary" className="cardText">
                      Sugar: {fruit.nutritions["sugar"]}
                    </ListGroup.Item>
                  </ListGroup>
                </Card.Body>
              </Card>
            );
          })}
        </Col>
      </Row>
    </Container>
  );
};

export default Fruits;
