import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import "./home.css";

const Home = () => {
  return (
    <Container>
      <Row style={{ marginTop: "40px"}}>
        <Col xs={12} md={12}>
          <h1 className="heading-home">TUTO</h1>
          <h1 className="heading-home">
            <span style={{ color: "#61DBFB" }}>REACT JS</span> and{" "}
            <span style={{ color: "#3C873A" }}>NODE JS</span>
            <span style={{ color: "#3F3E42" }}>JSON DATA</span>
          </h1>
        </Col>
      </Row>
    </Container>
  );
};

export default Home;
