import React, { useState, useEffect } from "react";
import axios from "axios";
import { Card, Col, Container, Row } from "react-bootstrap";
import microImgTest from "../../assets/micronutriments/nature-img.jpg";
import { URL } from '../../url';
import "./microNutrients.css";

const MicroNutrients = () => {
  const [microNutrients, setMicroNutrients] = useState([]);
  const [selectedMicroNutrients, setSelectedMicroNutrients] = useState(microNutrients);

  useEffect(() => {
    // fetch data
    getAllMicroNutrients();
  }, []);

  const getAllMicroNutrients = () => {
    axios
      .get(`${URL}/micro-nutrients`)
      .then((response) => {
        const allMicroNutrients = response.data;
        //console.log("data from api: ", allMicroNutrients);

        // add our data to state
        setMicroNutrients(allMicroNutrients);
        setSelectedMicroNutrients(allMicroNutrients);
      })
      .catch((error) => console.error(`Error: ${error}`));
  };
  
  const handleSelect = (e) => {
    // option value when selected
    const selectedValue = e.target.value;

    // store selected value in array
    let result = [];
    result = microNutrients.filter((data) => {
      if (selectedValue === "All") {
        return selectedMicroNutrients;
      }
      return data.commercialName.search(selectedValue) != -1;
      
    });

    setSelectedMicroNutrients(result);
  };
  

  return (
    <Container>
      <Row className="cardRow">
        <Col md={3} className="selectCol">
          <select
            value={selectedMicroNutrients.commercialName}
            onChange={handleSelect}
            className="form-select mx-auto form-select-md mb-3 bg-warning"
            aria-label=".form-select-lg example"
          >
            <option value="All">All</option>
            {microNutrients.map((nutrients) => (
              <>
              <option key={nutrients.id} value={nutrients.commercialName}>
                {nutrients.commercialName}
              </option>
              </>
            ))}
          </select>
        </Col>

        <Col md={9} className="cardCol">
          {selectedMicroNutrients.map((val, index) => (
            <Card className="card" style={{ backgroundColor: val.color }}>
              <Card.Img
                variant="top"
                src={microImgTest}
                className="cardImage"
              />
              <Card.Body>
                <Card.Title>{val.commercialName}</Card.Title>
                <Card.Text className="cardText">
                  {val.refillDescription}
                </Card.Text>
              </Card.Body>
            </Card>
          ))}
        </Col>
      </Row>
    </Container>
  );
};

export default MicroNutrients;
